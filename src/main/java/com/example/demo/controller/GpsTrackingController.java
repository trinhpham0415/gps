package com.example.demo.controller;

import com.example.demo.common.Constants;
import com.example.demo.common.TemplateConstants;
import com.example.demo.common.UriConstants;
import com.example.demo.domain.entity.Gps;
import com.example.demo.service.GpsService;
import com.example.demo.service.StorageService;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping(UriConstants.GPS_TRACKS_URI)
public class GpsTrackingController {

  private final StorageService storageService;
  private final GpsService gpsService;

  @Autowired
  public GpsTrackingController(StorageService storageService, GpsService gpsService) {
    this.storageService = storageService;
    this.gpsService = gpsService;
  }

  @GetMapping(UriConstants.GPS_TRACK_UPLOAD_URI)
  public String getUploadGpsTrackPage() {
    return TemplateConstants.UPLOAD_GPS_TRACK_PAGE;
  }

  @PostMapping
  public String uploadGpsTracks(@RequestParam(Constants.FILE) MultipartFile file) {
    //store file
    Path filePath = storageService.store(file);
    //add Gps
    gpsService.addGps(filePath.toString());

    return TemplateConstants.GPS_TRACKING_FILES_REDIRECT_TEMPLATE;
  }

  @GetMapping
  public String getAllGpsTracks(@RequestParam("page") @Min(value = 1) int page, Model model) {
    Page<Gps> gpsData = gpsService.getAllGps(page - 1);
    int totalPages = gpsData.getTotalPages();

    if (totalPages > 0) {
      List<Integer> pages = IntStream.rangeClosed(1, totalPages)
          .boxed()
          .collect(Collectors.toList());
      model.addAttribute("pages", pages);
    }

    model.addAttribute("gpsData", gpsData);

    return TemplateConstants.GPS_TRACKS_PAGE;
  }

  @GetMapping(Constants.ID_PATH_VARIABLE)
  public String getGpsTrackDetail(@PathVariable(Constants.ID) String gpsId, Model model) {
    model.addAttribute(Constants.GPS, gpsService.getById(UUID.fromString(gpsId)));
    return TemplateConstants.GPS_TRACK_DETAIL_PAGE;
  }

}
