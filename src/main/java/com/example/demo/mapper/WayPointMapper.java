package com.example.demo.mapper;

import com.example.demo.domain.entity.Gps;
import com.example.demo.domain.entity.WayPoint;
import com.example.demo.domain.xml.Gpx;
import com.example.demo.domain.xml.Wpt;
import java.util.ArrayList;
import java.util.List;

public class WayPointMapper {

  public static List<WayPoint> extractFromGpx(Gpx gpx, Gps gps) {
    List<WayPoint> destination = new ArrayList<>();
    gpx.getWpt().forEach(e -> {
      destination.add(extractFrom(e, gps));
    });

    return destination;
  }

  private static WayPoint extractFrom(Wpt source, Gps gps) {
    WayPoint destination = new WayPoint();
    destination.setLat(source.getLat());
    destination.setLon(source.getLon());
    destination.setName(source.getName());
    destination.setSym(source.getSym());
    destination.setGps(gps);

    return destination;
  }
}
