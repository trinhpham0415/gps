package com.example.demo.mapper;

import com.example.demo.domain.entity.Gps;
import com.example.demo.domain.entity.Metadata;
import com.example.demo.domain.xml.Gpx;

public class MetadataMapper {

  public static Metadata extractFromGpx(Gpx gpx, Gps gps) {
    Metadata destination = new Metadata();
    com.example.demo.domain.xml.Metadata source = gpx.getMetadata();
    destination.setName(source.getName());
    destination.setDesc(source.getDesc());
    destination.setAuthor(source.getAuthor());
    destination.setLinkHref(source.getLink().getHref());
    destination.setLinkText(source.getLink().getText());
    destination.setTime(source.getTime());
    destination.setGps(gps);

    return destination;
  }
}
