package com.example.demo.mapper;

import com.example.demo.domain.entity.Gps;
import com.example.demo.domain.entity.TrackPoint;
import com.example.demo.domain.xml.Gpx;
import com.example.demo.domain.xml.Trkpt;
import java.util.ArrayList;
import java.util.List;

public class TrackPointMapper {

  public static List<TrackPoint> extractFromGpx(Gpx gpx, Gps gps) {
    List<TrackPoint> destination = new ArrayList<>();
    gpx.getTrk().getTrkseg().getTrkpt().forEach(e -> {
      destination.add(extractFrom(e, gps));
    });

    return destination;
  }

  private static TrackPoint extractFrom(Trkpt source, Gps gps) {
    TrackPoint destination = new TrackPoint();
    destination.setEle(source.getEle());
    destination.setLat(source.getLat());
    destination.setLon(source.getLon());
    destination.setTime(source.getTime());
    destination.setGps(gps);

    return destination;
  }
}
