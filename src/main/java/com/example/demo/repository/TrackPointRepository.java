package com.example.demo.repository;

import com.example.demo.domain.entity.TrackPoint;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrackPointRepository extends JpaRepository<TrackPoint, UUID> {
}
