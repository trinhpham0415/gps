package com.example.demo.repository;

import com.example.demo.domain.entity.WayPoint;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WayPointRepository extends JpaRepository<WayPoint, UUID> {
}
