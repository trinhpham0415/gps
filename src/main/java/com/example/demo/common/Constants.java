package com.example.demo.common;

public class Constants {
  public static final String FILE = "file";
  public static final String FILES = "files";
  public static final String ID = "id";
  public static final String ID_PATH_VARIABLE = "/{id}";
  public static final String GPS = "gps";
  public static final String GPS_ID = "gps_id";
  public static final String PAGE_SIZE = "${paging.pageSize}";
}
