package com.example.demo.common;

public class TemplateConstants {

  public static final String UPLOAD_GPS_TRACK_PAGE = "uploadGpsTrack";
  public static final String GPS_TRACKS_PAGE = "gpsTracks";
  public static final String GPS_TRACK_DETAIL_PAGE = "gpsTrackDetail";
  public static final String GPS_TRACKING_FILES_REDIRECT_TEMPLATE =
      "redirect:" + UriConstants.GPS_TRACKS_URI + UriConstants.GPS_TRACK_UPLOAD_URI;
}
