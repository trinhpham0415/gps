package com.example.demo.domain.entity;

import com.example.demo.common.Constants;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
public class Metadata {
  @Id
  @Column(name = Constants.GPS_ID)
  private UUID id;
  private String name;
  @Column(columnDefinition="TEXT")
  private String desc;
  private String author;
  private String linkHref;
  private String linkText;
  private Date time;
  @OneToOne
  @MapsId
  @JoinColumn(name = Constants.GPS_ID)
  private Gps gps;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getLinkHref() {
    return linkHref;
  }

  public void setLinkHref(String linkHref) {
    this.linkHref = linkHref;
  }

  public String getLinkText() {
    return linkText;
  }

  public void setLinkText(String linkText) {
    this.linkText = linkText;
  }

  public Gps getGps() {
    return gps;
  }

  public void setGps(Gps gps) {
    this.gps = gps;
  }
}
