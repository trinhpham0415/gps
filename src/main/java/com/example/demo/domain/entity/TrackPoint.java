package com.example.demo.domain.entity;

import com.example.demo.common.Constants;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class TrackPoint {
  @Id
  @GeneratedValue
  private UUID id;
  private String lat;
  private String lon;
  private double ele;
  private Date time;
  @ManyToOne
  @JoinColumn(name = Constants.GPS_ID)
  private Gps gps;

  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getLon() {
    return lon;
  }

  public void setLon(String lon) {
    this.lon = lon;
  }

  public double getEle() {
    return ele;
  }

  public void setEle(double ele) {
    this.ele = ele;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Gps getGps() {
    return gps;
  }

  public void setGps(Gps gps) {
    this.gps = gps;
  }

  @Override
  public String toString() {
    return "latitude: '" + lat + '\'' +
        ", longitude: '" + lon + '\'' +
        ", ele: " + ele +
        ", time: " + time;
  }
}
