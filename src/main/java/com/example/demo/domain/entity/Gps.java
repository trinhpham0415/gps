package com.example.demo.domain.entity;

import com.example.demo.common.Constants;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Gps {
  @Id
  @GeneratedValue
  private UUID id;
  @OneToOne(mappedBy = Constants.GPS)
  @PrimaryKeyJoinColumn
  private Metadata metadata;
  @OneToMany(mappedBy = Constants.GPS)
  private Set<WayPoint> wayPoints;
  @OneToMany(mappedBy = Constants.GPS)
  private Set<TrackPoint> trackPoints;
  private String filePath;

  public Metadata getMetadata() {
    return metadata;
  }

  public void setMetadata(Metadata metadata) {
    this.metadata = metadata;
  }

  public Set<WayPoint> getWpt() {
    return wayPoints;
  }

  public void setWpt(Set<WayPoint> wayPointList) {
    this.wayPoints = wayPointList;
  }

  public Set<TrackPoint> getTrk() {
    return trackPoints;
  }

  public void setTrk(Set<TrackPoint> trackPointList) {
    this.trackPoints = trackPointList;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Set<WayPoint> getWayPoints() {
    return wayPoints;
  }

  public void setWayPoints(Set<WayPoint> wayPoints) {
    this.wayPoints = wayPoints;
  }

  public Set<TrackPoint> getTrackPoints() {
    return trackPoints;
  }

  public void setTrackPoints(Set<TrackPoint> trackPoints) {
    this.trackPoints = trackPoints;
  }
}
