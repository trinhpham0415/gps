package com.example.demo.domain.xml;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"name", "desc", "author", "link", "time"})
public class Metadata {
  private String name;
  private String desc;
  private String author;
  private Link link;
  private Date time;

  public String getName() {
    return name;
  }

  @XmlElement
  public void setName(String name) {
    this.name = name;
  }

  public String getDesc() {
    return desc;
  }

  @XmlElement
  public void setDesc(String desc) {
    this.desc = desc;
  }

  public String getAuthor() {
    return author;
  }

  @XmlElement
  public void setAuthor(String author) {
    this.author = author;
  }

  public Link getLink() {
    return link;
  }

  @XmlElement
  public void setLink(Link link) {
    this.link = link;
  }

  public Date getTime() {
    return time;
  }

  @XmlElement
  public void setTime(Date time) {
    this.time = time;
  }
}
