package com.example.demo.domain.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "gpx", namespace = "http://www.topografix.com/GPX/1/1")
//@XmlType(propOrder = {"xmlns", "version", "creator", "metadata", "wpt", "trk"})
@XmlType(propOrder = {"version", "creator", "metadata", "wpt", "trk"})
public class Gpx {
//  private String xmlns;
  private String version;
  private String creator;
  private Metadata metadata;
  private List<Wpt> wpt;
  private Trk trk;

//  public String getXmlns() {
//    return xmlns;
//  }
//
//  @XmlAttribute
//  public void setXmlns(String xmlns) {
//    this.xmlns = xmlns;
//  }

  public String getVersion() {
    return version;
  }

  @XmlAttribute
  public void setVersion(String version) {
    this.version = version;
  }

  public String getCreator() {
    return creator;
  }

  @XmlAttribute
  public void setCreator(String creator) {
    this.creator = creator;
  }

  public Metadata getMetadata() {
    return metadata;
  }

  @XmlElement
  public void setMetadata(Metadata metadata) {
    this.metadata = metadata;
  }

  public List<Wpt> getWpt() {
    return wpt;
  }

  @XmlElement
  public void setWpt(List<Wpt> wpt) {
    this.wpt = wpt;
  }

  public Trk getTrk() {
    return trk;
  }

  @XmlElement
  public void setTrk(Trk trk) {
    this.trk = trk;
  }
}
