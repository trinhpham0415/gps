package com.example.demo.domain.xml;

import java.util.Date;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"lat", "lon", "ele", "time"})
public class Trkpt {
  private String lat;
  private String lon;
  private double ele;
  private Date time;

  public String getLat() {
    return lat;
  }

  @XmlAttribute
  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getLon() {
    return lon;
  }

  @XmlAttribute
  public void setLon(String lon) {
    this.lon = lon;
  }

  public double getEle() {
    return ele;
  }

  @XmlElement
  public void setEle(double ele) {
    this.ele = ele;
  }

  public Date getTime() {
    return time;
  }

  @XmlElement
  public void setTime(Date time) {
    this.time = time;
  }
}
