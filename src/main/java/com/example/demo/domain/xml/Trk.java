package com.example.demo.domain.xml;

import javax.xml.bind.annotation.XmlElement;

public class Trk {
  private Trkseg trkseg;

  public Trkseg getTrkseg() {
    return trkseg;
  }

  @XmlElement
  public void setTrkseg(Trkseg trkseg) {
    this.trkseg = trkseg;
  }
}
