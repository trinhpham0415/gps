package com.example.demo.domain.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"href", "text"})
public class Link {
  private String href;
  private String text;

  public String getHref() {
    return href;
  }

  @XmlAttribute
  public void setHref(String href) {
    this.href = href;
  }

  public String getText() {
    return text;
  }

  @XmlElement
  public void setText(String text) {
    this.text = text;
  }
}
