package com.example.demo.domain.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"lat", "lon", "name", "sym"})
public class Wpt {
  private String lat;
  private String lon;
  private String name;
  private String sym;

  public String getLat() {
    return lat;
  }

  @XmlAttribute
  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getLon() {
    return lon;
  }

  @XmlAttribute
  public void setLon(String lon) {
    this.lon = lon;
  }

  public String getName() {
    return name;
  }

  @XmlElement
  public void setName(String name) {
    this.name = name;
  }

  public String getSym() {
    return sym;
  }

  @XmlElement
  public void setSym(String sym) {
    this.sym = sym;
  }
}
