package com.example.demo.domain.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;

public class Trkseg {
  private List<Trkpt> trkpt;

  public List<Trkpt> getTrkpt() {
    return trkpt;
  }

  @XmlElement
  public void setTrkpt(List<Trkpt> trkpt) {
    this.trkpt = trkpt;
  }
}
