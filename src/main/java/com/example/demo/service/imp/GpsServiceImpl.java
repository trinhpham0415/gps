package com.example.demo.service.imp;

import com.example.demo.common.Constants;
import com.example.demo.domain.entity.Gps;
import com.example.demo.domain.entity.Metadata;
import com.example.demo.domain.entity.TrackPoint;
import com.example.demo.domain.entity.WayPoint;
import com.example.demo.domain.xml.Gpx;
import com.example.demo.mapper.MetadataMapper;
import com.example.demo.mapper.TrackPointMapper;
import com.example.demo.mapper.WayPointMapper;
import com.example.demo.repository.GpsRepository;
import com.example.demo.repository.MetadataRepository;
import com.example.demo.repository.TrackPointRepository;
import com.example.demo.repository.WayPointRepository;
import com.example.demo.service.GpsService;
import com.example.demo.service.XmlService;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class GpsServiceImpl implements GpsService {

  private final XmlService xmlService;
  private final GpsRepository gpsRepository;
  private final MetadataRepository metadataRepository;
  private final TrackPointRepository trackPointRepository;
  private final WayPointRepository wayPointRepository;
  @Value(Constants.PAGE_SIZE)
  private int pageSize;

  @Autowired
  public GpsServiceImpl(XmlService xmlService, GpsRepository gpsRepository,
         MetadataRepository metadataRepository,
         TrackPointRepository trackPointRepository, WayPointRepository wayPointRepository) {
    this.xmlService = xmlService;
    this.gpsRepository = gpsRepository;
    this.metadataRepository = metadataRepository;
    this.trackPointRepository = trackPointRepository;
    this.wayPointRepository = wayPointRepository;
  }

  @Override
  public void addGps(String filePath) {
    //read file
    Gpx gpx = null;
    try {
      gpx = xmlService.unmarshall(filePath.toString());
    } catch (JAXBException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    Gps gps = new Gps();
    gps.setFilePath(filePath);

    Metadata metadata = MetadataMapper.extractFromGpx(gpx, gps);
    List<TrackPoint> trackPoints = TrackPointMapper.extractFromGpx(gpx, gps);
    List<WayPoint> wayPoints = WayPointMapper.extractFromGpx(gpx, gps);

    metadataRepository.save(metadata);
    trackPointRepository.save(trackPoints);
    wayPointRepository.save(wayPoints);
  }

  @Override
  public Page<Gps> getAllGps(int page) {
    return gpsRepository.findAll(new PageRequest(page, pageSize));
  }

  @Override
  public Gps getById(UUID id) {
    return gpsRepository.findOne(id);
  }
}
