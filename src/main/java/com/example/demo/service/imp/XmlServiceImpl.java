package com.example.demo.service.imp;

import com.example.demo.domain.xml.Gpx;
import com.example.demo.service.XmlService;
import java.io.FileReader;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import org.springframework.stereotype.Service;

@Service
public class XmlServiceImpl implements XmlService {

  @Override
  public Gpx unmarshall(String filePath) throws JAXBException, IOException {
    JAXBContext jaxbContext = JAXBContext.newInstance(Gpx.class);

    return (Gpx) jaxbContext.createUnmarshaller().unmarshal(
//        new FileReader("sample/sample_2.gpx")
        new FileReader(filePath)
    );
  }

}
