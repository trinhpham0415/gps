package com.example.demo.service;

import com.example.demo.domain.entity.Gps;
import java.util.UUID;
import org.springframework.data.domain.Page;

public interface GpsService {

  void addGps(String filePath);

  Page<Gps> getAllGps(int page);

  Gps getById(UUID id);

}
