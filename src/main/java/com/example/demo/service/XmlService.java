package com.example.demo.service;

import com.example.demo.domain.xml.Gpx;
import java.io.IOException;
import javax.xml.bind.JAXBException;

public interface XmlService {

  Gpx unmarshall(String filePath) throws JAXBException, IOException;

}
